-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 25, 2018 at 05:02 PM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `financierd_opalcrm`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity_logs`
--

CREATE TABLE `activity_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `login_as_user_id` int(10) UNSIGNED DEFAULT NULL,
  `user_agent` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `module` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `module_id` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_module` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_module_id` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activity` text COLLATE utf8mb4_unicode_ci,
  `ip` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `activity_logs`
--

INSERT INTO `activity_logs` (`id`, `user_id`, `login_as_user_id`, `user_agent`, `module`, `module_id`, `sub_module`, `sub_module_id`, `activity`, `ip`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'user', '1', NULL, NULL, 'logged_in', '127.0.0.1', '2018-09-21 05:53:48', '2018-09-21 05:53:48'),
(2, 1, NULL, 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'todo', '1', NULL, NULL, 'added', '127.0.0.1', '2018-09-21 11:44:57', '2018-09-21 11:44:57'),
(3, 1, NULL, 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'todo', '2', NULL, NULL, 'added', '127.0.0.1', '2018-09-21 11:53:30', '2018-09-21 11:53:30'),
(4, 1, NULL, 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'todo', '3', NULL, NULL, 'added', '127.0.0.1', '2018-09-21 11:53:44', '2018-09-21 11:53:44'),
(5, 1, NULL, 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'todo', '3', NULL, NULL, 'updated', '127.0.0.1', '2018-09-21 14:55:02', '2018-09-21 14:55:02'),
(6, 1, NULL, 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'user', '1', NULL, NULL, 'logged_out', '127.0.0.1', '2018-09-21 15:01:14', '2018-09-21 15:01:14'),
(7, 1, NULL, 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'user', '1', NULL, NULL, 'logged_in', '127.0.0.1', '2018-09-21 15:02:27', '2018-09-21 15:02:27'),
(8, 1, NULL, 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'user', '2', NULL, NULL, 'updated', '127.0.0.1', '2018-09-21 15:03:35', '2018-09-21 15:03:35'),
(9, 1, NULL, 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'todo', '4', NULL, NULL, 'added', '127.0.0.1', '2018-09-21 17:30:21', '2018-09-21 17:30:21'),
(10, 1, NULL, 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'todo', '5', NULL, NULL, 'added', '127.0.0.1', '2018-09-21 17:30:33', '2018-09-21 17:30:33'),
(11, 1, NULL, 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'todo', '4', NULL, NULL, 'updated', '127.0.0.1', '2018-09-21 17:30:49', '2018-09-21 17:30:49'),
(12, 1, NULL, 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'todo', '5', NULL, NULL, 'updated', '127.0.0.1', '2018-09-21 17:31:04', '2018-09-21 17:31:04'),
(13, 1, NULL, 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'todo', '5', NULL, NULL, 'updated', '127.0.0.1', '2018-09-21 17:31:09', '2018-09-21 17:31:09'),
(14, 1, NULL, 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'user', '1', NULL, NULL, 'logged_in', '183.83.111.122', '2018-09-22 06:48:56', '2018-09-22 06:48:56'),
(15, 1, NULL, 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'todo', '6', NULL, NULL, 'added', '183.83.111.122', '2018-09-22 06:49:26', '2018-09-22 06:49:26'),
(16, 1, NULL, 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'todo', '7', NULL, NULL, 'added', '183.83.111.122', '2018-09-22 06:49:35', '2018-09-22 06:49:35'),
(17, 1, NULL, 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'todo', '6', NULL, NULL, 'updated', '183.83.111.122', '2018-09-22 06:49:38', '2018-09-22 06:49:38'),
(18, 1, NULL, 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'todo', '7', NULL, NULL, 'updated', '183.83.111.122', '2018-09-22 06:49:45', '2018-09-22 06:49:45'),
(19, 1, NULL, 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'user', '1', NULL, NULL, 'logged_out', '183.83.111.122', '2018-09-22 06:49:49', '2018-09-22 06:49:49'),
(20, 1, NULL, 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'user', '1', NULL, NULL, 'logged_in', '183.83.111.122', '2018-09-22 06:50:08', '2018-09-22 06:50:08'),
(21, 1, NULL, 'Mozilla/5.0 (Linux; Android 8.0.0; SM-G955F Build/R16NW) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Mobile Safari/537.36', 'user', '1', NULL, NULL, 'logged_in', '62.140.132.17', '2018-09-22 09:29:54', '2018-09-22 09:29:54'),
(22, 1, NULL, 'Mozilla/5.0 (Linux; Android 8.0.0; SM-G955F Build/R16NW) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Mobile Safari/537.36', 'user', '1', NULL, NULL, 'logged_out', '62.140.132.17', '2018-09-22 10:02:27', '2018-09-22 10:02:27'),
(23, 1, NULL, 'Mozilla/5.0 (Linux; Android 8.0.0; SM-G955F Build/R16NW) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Mobile Safari/537.36', 'user', '1', NULL, NULL, 'logged_in', '62.140.132.17', '2018-09-22 10:05:40', '2018-09-22 10:05:40'),
(24, 1, NULL, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'user', '1', NULL, NULL, 'logged_in', '213.127.113.199', '2018-09-22 12:42:13', '2018-09-22 12:42:13'),
(25, 1, NULL, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'user', '1', NULL, NULL, 'logged_out', '213.127.113.199', '2018-09-22 12:43:15', '2018-09-22 12:43:15'),
(26, 1, NULL, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'user', '1', NULL, NULL, 'logged_in', '213.127.113.199', '2018-09-22 12:43:17', '2018-09-22 12:43:17'),
(27, 1, NULL, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'configuration', NULL, 'system', NULL, 'saved', '213.127.113.199', '2018-09-22 12:48:53', '2018-09-22 12:48:53'),
(28, 1, NULL, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'user', '1', NULL, NULL, 'logged_out', '213.127.113.199', '2018-09-22 12:48:59', '2018-09-22 12:48:59'),
(29, 1, NULL, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'user', '1', NULL, NULL, 'logged_in', '213.127.113.199', '2018-09-22 12:49:05', '2018-09-22 12:49:05'),
(30, 1, NULL, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'configuration', NULL, 'system', NULL, 'saved', '213.127.113.199', '2018-09-22 12:49:55', '2018-09-22 12:49:55'),
(31, 1, NULL, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'user', '2', NULL, NULL, 'updated', '213.127.113.199', '2018-09-22 09:23:08', '2018-09-22 09:23:08'),
(32, 1, NULL, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'user', '2', NULL, NULL, 'updated', '213.127.113.199', '2018-09-22 09:23:11', '2018-09-22 09:23:11'),
(33, 1, NULL, 'Mozilla/5.0 (Linux; Android 7.1.1; SM-T555 Build/NMF26X) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.125 Safari/537.36', 'user', '1', NULL, NULL, 'logged_in', '213.127.113.199', '2018-09-22 14:48:59', '2018-09-22 14:48:59'),
(34, 1, NULL, 'Mozilla/5.0 (Linux; Android 8.0.0; SM-G955F Build/R16NW) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Mobile Safari/537.36', 'user', '1', NULL, NULL, 'logged_in', '62.72.192.56', '2018-09-23 11:29:01', '2018-09-23 11:29:01'),
(35, 1, NULL, 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'user', '1', NULL, NULL, 'logged_in', '183.83.109.121', '2018-09-24 06:42:37', '2018-09-24 06:42:37'),
(36, 1, NULL, 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'user', '1', NULL, NULL, 'logged_out', '183.83.109.121', '2018-09-24 06:42:51', '2018-09-24 06:42:51'),
(37, 1, NULL, 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'user', '1', NULL, NULL, 'logged_in', '183.83.109.121', '2018-09-24 06:44:57', '2018-09-24 06:44:57'),
(38, 1, NULL, 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'user', '1', 'password', NULL, 'resetted', '183.83.109.121', '2018-09-24 06:48:16', '2018-09-24 06:48:16'),
(39, 1, NULL, 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'user', '1', NULL, NULL, 'logged_out', '183.83.109.121', '2018-09-24 06:48:26', '2018-09-24 06:48:26'),
(40, 1, NULL, 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'user', '1', NULL, NULL, 'logged_in', '183.83.109.121', '2018-09-24 06:48:52', '2018-09-24 06:48:52'),
(41, 1, NULL, 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'permission', NULL, NULL, NULL, 'assigned', '183.83.109.121', '2018-09-24 06:50:26', '2018-09-24 06:50:26'),
(42, 1, NULL, 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'user', '1', NULL, NULL, 'logged_out', '183.83.109.121', '2018-09-24 06:52:08', '2018-09-24 06:52:08'),
(43, 2, NULL, 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'user', '2', NULL, NULL, 'logged_in', '183.83.109.121', '2018-09-24 06:52:34', '2018-09-24 06:52:34'),
(44, 2, NULL, 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'user', '2', 'password', NULL, 'resetted', '183.83.109.121', '2018-09-24 06:52:54', '2018-09-24 06:52:54'),
(45, 2, NULL, 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'user', '2', NULL, NULL, 'logged_out', '183.83.109.121', '2018-09-24 06:53:05', '2018-09-24 06:53:05'),
(46, 2, NULL, 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'user', '2', NULL, NULL, 'logged_in', '183.83.109.121', '2018-09-24 06:53:13', '2018-09-24 06:53:13'),
(47, 2, NULL, 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'user', '2', NULL, NULL, 'logged_out', '183.83.109.121', '2018-09-24 07:59:10', '2018-09-24 07:59:10'),
(48, 1, NULL, 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'user', '1', NULL, NULL, 'logged_in', '183.83.109.121', '2018-09-24 07:59:22', '2018-09-24 07:59:22'),
(49, 1, NULL, 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'user', '1', NULL, NULL, 'logged_in', '183.83.109.121', '2018-09-24 08:00:24', '2018-09-24 08:00:24'),
(50, 1, NULL, 'Mozilla/5.0 (Linux; Android 8.0.0; SM-G955F Build/R16NW) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Mobile Safari/537.36', 'user', '1', NULL, NULL, 'logged_in', '62.72.192.56', '2018-09-24 08:13:52', '2018-09-24 08:13:52'),
(51, 1, NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'user', '1', NULL, NULL, 'logged_out', '62.72.192.56', '2018-09-24 08:15:41', '2018-09-24 08:15:41'),
(52, 1, NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'user', '1', NULL, NULL, 'logged_in', '62.72.192.56', '2018-09-24 08:17:00', '2018-09-24 08:17:00'),
(53, 1, NULL, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'user', '1', NULL, NULL, 'logged_in', '213.127.86.148', '2018-09-24 09:00:39', '2018-09-24 09:00:39'),
(54, 2, NULL, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'user', '2', NULL, NULL, 'logged_in', '213.127.86.148', '2018-09-24 09:32:08', '2018-09-24 09:32:08'),
(55, 2, NULL, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'user', '2', 'avatar', NULL, 'uploaded', '213.127.86.148', '2018-09-24 09:32:48', '2018-09-24 09:32:48'),
(56, 1, NULL, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'todo', '8', NULL, NULL, 'added', '213.127.86.148', '2018-09-24 09:39:54', '2018-09-24 09:39:54'),
(57, 1, NULL, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36 OPR/55.0.2994.61', 'user', '1', NULL, NULL, 'logged_in', '209.95.60.151', '2018-09-24 10:11:31', '2018-09-24 10:11:31'),
(58, 1, NULL, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36 OPR/55.0.2994.61', 'todo', '9', NULL, NULL, 'added', '209.95.60.151', '2018-09-24 10:55:20', '2018-09-24 10:55:20'),
(59, 1, NULL, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'user', '1', NULL, NULL, 'logged_in', '127.0.0.1', '2018-09-24 07:00:23', '2018-09-24 07:00:23'),
(60, 1, NULL, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'user', '1', NULL, NULL, 'logged_out', '127.0.0.1', '2018-09-24 07:48:43', '2018-09-24 07:48:43'),
(61, 1, NULL, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'user', '1', NULL, NULL, 'logged_in', '127.0.0.1', '2018-09-24 07:48:51', '2018-09-24 07:48:51'),
(62, 1, NULL, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'user', '1', NULL, NULL, 'logged_in', '127.0.0.1', '2018-09-24 09:37:48', '2018-09-24 09:37:48'),
(63, 1, NULL, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'user', '1', NULL, NULL, 'logged_in', '127.0.0.1', '2018-09-24 09:50:28', '2018-09-24 09:50:28'),
(64, 1, NULL, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'user', '1', NULL, NULL, 'logged_in', '127.0.0.1', '2018-09-24 09:59:06', '2018-09-24 09:59:06'),
(65, 1, NULL, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'user', '1', NULL, NULL, 'logged_in', '127.0.0.1', '2018-09-24 10:00:05', '2018-09-24 10:00:05'),
(66, 1, NULL, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'todo', '9', NULL, NULL, 'updated', '127.0.0.1', '2018-09-24 10:06:23', '2018-09-24 10:06:23'),
(67, 1, NULL, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'todo', '9', NULL, NULL, 'updated', '127.0.0.1', '2018-09-24 10:06:25', '2018-09-24 10:06:25'),
(68, 1, NULL, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'user', '1', NULL, NULL, 'logged_in', '127.0.0.1', '2018-09-24 10:28:16', '2018-09-24 10:28:16'),
(69, 1, NULL, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'user', '1', NULL, NULL, 'logged_in', '127.0.0.1', '2018-09-24 10:38:32', '2018-09-24 10:38:32'),
(70, 1, NULL, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'user', '1', NULL, NULL, 'logged_in', '127.0.0.1', '2018-09-24 10:39:26', '2018-09-24 10:39:26'),
(71, 1, NULL, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'user', '1', NULL, NULL, 'logged_in', '127.0.0.1', '2018-09-24 10:54:00', '2018-09-24 10:54:00'),
(72, 1, NULL, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'user', '1', NULL, NULL, 'logged_in', '127.0.0.1', '2018-09-24 11:07:30', '2018-09-24 11:07:30'),
(73, 1, NULL, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'message', '1', NULL, NULL, 'sent', '127.0.0.1', '2018-09-24 13:31:10', '2018-09-24 13:31:10'),
(74, 1, NULL, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'user', '1', NULL, NULL, 'logged_in', '127.0.0.1', '2018-09-24 22:51:33', '2018-09-24 22:51:33'),
(75, 1, NULL, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'user', '1', NULL, NULL, 'logged_in', '127.0.0.1', '2018-09-25 01:06:34', '2018-09-25 01:06:34'),
(76, 1, NULL, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'user', '1', 'profile', NULL, 'updated', '127.0.0.1', '2018-09-25 01:22:06', '2018-09-25 01:22:06'),
(77, 1, NULL, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'user', '1', NULL, NULL, 'logged_out', '127.0.0.1', '2018-09-25 02:51:40', '2018-09-25 02:51:40'),
(78, 1, NULL, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'user', '1', NULL, NULL, 'logged_in', '127.0.0.1', '2018-09-25 02:51:42', '2018-09-25 02:51:42'),
(79, 1, NULL, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'user', '1', 'avatar', NULL, 'uploaded', '127.0.0.1', '2018-09-25 04:30:12', '2018-09-25 04:30:12'),
(80, 1, NULL, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'user', '1', 'avatar', NULL, 'removed', '127.0.0.1', '2018-09-25 04:30:24', '2018-09-25 04:30:24'),
(81, 1, NULL, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'user', '1', 'avatar', NULL, 'uploaded', '127.0.0.1', '2018-09-25 04:30:46', '2018-09-25 04:30:46'),
(82, 1, NULL, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'user', '1', 'avatar', NULL, 'removed', '127.0.0.1', '2018-09-25 04:32:37', '2018-09-25 04:32:37'),
(83, 1, NULL, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'user', '1', 'avatar', NULL, 'uploaded', '127.0.0.1', '2018-09-25 04:32:44', '2018-09-25 04:32:44');

-- --------------------------------------------------------

--
-- Table structure for table `backups`
--

CREATE TABLE `backups` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `chats`
--

CREATE TABLE `chats` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `config`
--

CREATE TABLE `config` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `numeric_value` bigint(20) DEFAULT NULL,
  `text_value` text COLLATE utf8mb4_unicode_ci,
  `is_private` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `config`
--

INSERT INTO `config` (`id`, `name`, `numeric_value`, `text_value`, `is_private`) VALUES
(1, 'color_theme', NULL, 'red', 0),
(2, 'direction', NULL, 'ltr', 0),
(3, 'sidebar', NULL, 'mini', 0),
(4, 'locale', NULL, 'en', 0),
(5, 'timezone', NULL, 'Europe/Amsterdam', 0),
(6, 'notification_position', NULL, 'toast-bottom-right', 0),
(7, 'date_format', NULL, 'DD-MM-YYYY', 0),
(8, 'time_format', NULL, 'H:mm', 0),
(9, 'page_length', 25, NULL, 0),
(10, 'driver', NULL, 'log', 0),
(11, 'from_address', NULL, 'hello@example.com', 0),
(12, 'from_name', NULL, 'Hello', 0),
(13, 'token_lifetime', 120, NULL, 0),
(14, 'reset_password_token_lifetime', 30, NULL, 0),
(15, 'activity_log', 1, NULL, 0),
(16, 'email_log', 1, NULL, 0),
(17, 'reset_password', 1, NULL, 0),
(18, 'registration', 1, NULL, 0),
(19, 'mode', 1, NULL, 0),
(20, 'footer_credit', NULL, '© 2018 Financierdirect.nl', 0),
(21, 'multilingual', 1, NULL, 0),
(22, 'ip_filter', 1, NULL, 0),
(23, 'email_template', 1, NULL, 0),
(24, 'message', 1, NULL, 0),
(25, 'backup', 1, NULL, 0),
(26, 'todo', 1, NULL, 0),
(27, 'show_user_menu', 1, NULL, 0),
(28, 'show_todo_menu', 1, NULL, 0),
(29, 'show_message_menu', 1, NULL, 0),
(30, 'show_configuration_menu', 1, NULL, 0),
(31, 'show_backup_menu', 1, NULL, 0),
(32, 'show_email_template_menu', 1, NULL, 0),
(33, 'show_email_log_menu', 1, NULL, 0),
(34, 'show_activity_log_menu', 1, NULL, 0),
(35, 'https', 0, NULL, 0),
(36, 'error_display', 0, NULL, 0),
(37, 'maintenance_mode', 0, NULL, 0),
(38, 'maintenance_mode_message', NULL, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `email_logs`
--

CREATE TABLE `email_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `module` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `module_id` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `email_templates`
--

CREATE TABLE `email_templates` (
  `id` int(10) UNSIGNED NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `email_templates`
--

INSERT INTO `email_templates` (`id`, `is_default`, `name`, `slug`, `category`, `subject`, `body`, `created_at`, `updated_at`) VALUES
(1, 1, 'Welcome Email User', 'welcome-email-user', 'user', 'Welcome Email User | [COMPANY_NAME]', '<div style=\"margin:0px; background: #f8f8f8; \">\r\n  <div width=\"100%\" style=\"background: #f8f8f8; padding: 0px 0px; font-family:arial; line-height:28px; height:100%;  width: 100%; color: #514d6a;\">\r\n    <div style=\"max-width: 700px; padding:50px 0;  margin: 0px auto; font-size: 14px\">\r\n      <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%; margin-bottom: 20px\">\r\n        <tbody>\r\n          <tr>\r\n            <td style=\"vertical-align: top; padding-bottom:30px;\" align=\"center\">[COMPANY_LOGO]</td>\r\n          </tr>\r\n          <tr>\r\n            <td><h5 style=\"text-align:center;\">Account Created</h5></td>\r\n          </tr>\r\n        </tbody>\r\n      </table>\r\n      <div style=\"padding: 40px; background: #fff;\">\r\n        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%;\">\r\n          <tbody>\r\n            <tr>\r\n              <td style=\"border-bottom:1px solid #f6f6f6;\"><h1 style=\"font-size:14px; font-family:arial; margin:0px; font-weight:bold;\">Dear [NAME],</h1>\r\n                <p style=\"margin-top:0px; color:#bbbbbb;\">Welcome to our company. Your account has been created. Please use below credentials to log into your account:</p><table class=\"table table-bordered\"><tbody><tr><td>Email</td><td>[EMAIL]</td></tr><tr><td>Password</td><td>[PASSWORD]</td></tr></tbody></table><p style=\"margin-top:0px; color:#bbbbbb;\"><br></p></td>\r\n            </tr>\r\n            <tr>\r\n              <td style=\"padding:10px 0 30px 0;\"><p>Have a Good Day!</p>\r\n                <b>- Best Wishes ([COMPANY_NAME])</b> </td>\r\n            </tr>\r\n          </tbody>\r\n        </table>\r\n      </div>\r\n      <div style=\"text-align: center; font-size: 12px; color: #b2b2b5; margin-top: 20px\">\r\n        <p> [COMPANY_NAME] <br>\r\n        [COMPANY_EMAIL] | [COMPANY_PHONE] | [COMPANY_WEBSITE]</p>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n', NULL, NULL),
(2, 1, 'Anniversary Email User', 'anniversary-email-user', 'user', 'Wish You a Very Happy Anniversary [NAME] | [COMPANY_NAME]', '<div style=\"margin:0px; background: #f8f8f8; \">\r\n  <div width=\"100%\" style=\"background: #f8f8f8; padding: 0px 0px; font-family:arial; line-height:28px; height:100%;  width: 100%; color: #514d6a;\">\r\n    <div style=\"max-width: 700px; padding:50px 0;  margin: 0px auto; font-size: 14px\">\r\n      <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%; margin-bottom: 20px\">\r\n        <tbody>\r\n          <tr>\r\n            <td style=\"vertical-align: top; padding-bottom:30px;\" align=\"center\">[COMPANY_LOGO]</td>\r\n          </tr>\r\n          <tr>\r\n            <td><h5 style=\"text-align:center;\">Happy Anniversary</h5></td>\r\n          </tr>\r\n        </tbody>\r\n      </table>\r\n      <div style=\"padding: 40px; background: #fff;\">\r\n        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%;\">\r\n          <tbody>\r\n            <tr>\r\n              <td style=\"border-bottom:1px solid #f6f6f6;\"><h1 style=\"font-size:14px; font-family:arial; margin:0px; font-weight:bold;\">Dear [NAME],</h1>\r\n                <p style=\"margin-top:0px; color:#bbbbbb;\">We wish you a Very Happy Anniversary.</p></td>\r\n            </tr>\r\n            <tr>\r\n              <td style=\"padding:10px 0 30px 0;\"><p>Have a Good Day!</p>\r\n                <b>- Best Wishes ([COMPANY_NAME])</b> </td>\r\n            </tr>\r\n          </tbody>\r\n        </table>\r\n      </div>\r\n      <div style=\"text-align: center; font-size: 12px; color: #b2b2b5; margin-top: 20px\">\r\n        <p> [COMPANY_NAME] <br>\r\n        [COMPANY_EMAIL] | [COMPANY_PHONE] | [COMPANY_WEBSITE]</p>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n', NULL, NULL),
(3, 1, 'Birthday Email User', 'birthday-email-user', 'user', 'Happy Birthday [NAME] | [COMPANY_NAME]', '<div style=\"margin:0px; background: #f8f8f8; \">\r\n  <div width=\"100%\" style=\"background: #f8f8f8; padding: 0px 0px; font-family:arial; line-height:28px; height:100%;  width: 100%; color: #514d6a;\">\r\n    <div style=\"max-width: 700px; padding:50px 0;  margin: 0px auto; font-size: 14px\">\r\n      <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%; margin-bottom: 20px\">\r\n        <tbody>\r\n          <tr>\r\n            <td style=\"vertical-align: top; padding-bottom:30px;\" align=\"center\">[COMPANY_LOGO]</td>\r\n          </tr>\r\n          <tr>\r\n            <td><h5 style=\"text-align:center;\">Happy Birthday</h5></td>\r\n          </tr>\r\n        </tbody>\r\n      </table>\r\n      <div style=\"padding: 40px; background: #fff;\">\r\n        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%;\">\r\n          <tbody>\r\n            <tr>\r\n              <td style=\"border-bottom:1px solid #f6f6f6;\"><h1 style=\"font-size:14px; font-family:arial; margin:0px; font-weight:bold;\">Dear [NAME],</h1>\r\n                <p style=\"margin-top:0px; color:#bbbbbb;\">We wish you a Very Happy Birthday.</p></td>\r\n            </tr>\r\n            <tr>\r\n              <td style=\"padding:10px 0 30px 0;\"><p>Have a Good Day!</p>\r\n                <b>- Best Wishes ([COMPANY_NAME])</b> </td>\r\n            </tr>\r\n          </tbody>\r\n        </table>\r\n      </div>\r\n      <div style=\"text-align: center; font-size: 12px; color: #b2b2b5; margin-top: 20px\">\r\n        <p> [COMPANY_NAME] <br>\r\n        [COMPANY_EMAIL] | [COMPANY_PHONE] | [COMPANY_WEBSITE]</p>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ip_filters`
--

CREATE TABLE `ip_filters` (
  `id` int(10) UNSIGNED NOT NULL,
  `start_ip` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `end_ip` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `locales`
--

CREATE TABLE `locales` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `locale` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `locales`
--

INSERT INTO `locales` (`id`, `name`, `locale`, `created_at`, `updated_at`) VALUES
(1, 'English', 'en', '2018-09-21 00:18:37', '2018-09-21 00:18:37');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `uuid` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_draft` tinyint(1) NOT NULL DEFAULT '0',
  `from_user_id` int(10) UNSIGNED NOT NULL,
  `to_user_id` int(10) UNSIGNED DEFAULT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8mb4_unicode_ci,
  `is_important_by_sender` tinyint(1) NOT NULL DEFAULT '0',
  `is_important_by_receiver` tinyint(1) NOT NULL DEFAULT '0',
  `is_read` tinyint(1) NOT NULL DEFAULT '0',
  `is_deleted_by_sender` tinyint(1) NOT NULL DEFAULT '0',
  `is_deleted_by_receiver` tinyint(1) NOT NULL DEFAULT '0',
  `reply_id` int(10) UNSIGNED DEFAULT NULL,
  `has_attachment` tinyint(1) NOT NULL DEFAULT '0',
  `upload_token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `uuid`, `is_draft`, `from_user_id`, `to_user_id`, `subject`, `body`, `is_important_by_sender`, `is_important_by_receiver`, `is_read`, `is_deleted_by_sender`, `is_deleted_by_receiver`, `reply_id`, `has_attachment`, `upload_token`, `created_at`, `updated_at`) VALUES
(1, '1623d455-76f3-4cdf-a5ed-cf75cfcc7fb9', 0, 1, 2, '123', '123', 1, 0, 0, 0, 0, NULL, 1, '9e7d06ea-9995-40ae-be22-4385e5c36481', '2018-09-24 13:31:10', '2018-09-24 13:31:13');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_10_27_053833_create_permission_tables', 1),
(4, '2017_10_27_074549_create_config_table', 1),
(5, '2017_10_27_074635_create_locales_table', 1),
(6, '2017_10_27_074811_create_backups_table', 1),
(7, '2017_10_27_110555_create_todos_table', 1),
(8, '2017_10_27_110722_create_activity_logs_table', 1),
(9, '2017_10_27_112211_create_chats_table', 1),
(10, '2017_10_27_114519_create_profiles_table', 1),
(11, '2017_10_27_121507_create_email_logs_table', 1),
(12, '2017_10_27_121726_create_ip_filters_table', 1),
(13, '2017_10_27_122411_create_messages_table', 1),
(14, '2017_10_27_122805_create_jobs_table', 1),
(15, '2017_10_27_130426_create_user_preferences_table', 1),
(16, '2017_11_03_035042_create_uploads_table', 1),
(17, '2017_11_13_100342_create_email_templates_table', 1),
(18, '2017_12_24_034437_create_relations', 1),
(19, '2018_06_02_074608_update_user_preferences_table_add_sidebar_column', 1),
(20, '2018_09_21_170202_change_default_todo_status', 2);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\User', 1),
(2, 'App\\User', 2),
(2, 'App\\User', 3),
(2, 'App\\User', 4);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'access-configuration', 'web', '2018-09-21 00:18:37', '2018-09-21 00:18:37'),
(2, 'list-user', 'web', '2018-09-21 00:18:37', '2018-09-21 00:18:37'),
(3, 'create-user', 'web', '2018-09-21 00:18:37', '2018-09-21 00:18:37'),
(4, 'edit-user', 'web', '2018-09-21 00:18:37', '2018-09-21 00:18:37'),
(5, 'delete-user', 'web', '2018-09-21 00:18:37', '2018-09-21 00:18:37'),
(6, 'force-reset-user-password', 'web', '2018-09-21 00:18:37', '2018-09-21 00:18:37'),
(7, 'email-user', 'web', '2018-09-21 00:18:37', '2018-09-21 00:18:37'),
(8, 'change-status-user', 'web', '2018-09-21 00:18:37', '2018-09-21 00:18:37'),
(9, 'access-message', 'web', '2018-09-21 00:18:37', '2018-09-21 00:18:37'),
(10, 'access-todo', 'web', '2018-09-21 00:18:37', '2018-09-21 00:18:37'),
(11, 'enable-login', 'web', '2018-09-21 00:18:37', '2018-09-21 00:18:37');

-- --------------------------------------------------------

--
-- Table structure for table `proefritten`
--

CREATE TABLE `proefritten` (
  `id` int(11) NOT NULL,
  `name` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `license_plate` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `license_number` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` date DEFAULT NULL,
  `begin_location` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `end_location` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_time` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `end_time` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Rijbewijs` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE `profiles` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider_unique_id` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `date_of_anniversary` date DEFAULT NULL,
  `address_line_1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_line_2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zipcode` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_id` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook_profile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter_profile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google_plus_profile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkedin_profile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`id`, `user_id`, `first_name`, `last_name`, `provider`, `provider_unique_id`, `gender`, `avatar`, `phone`, `date_of_birth`, `date_of_anniversary`, `address_line_1`, `address_line_2`, `city`, `state`, `zipcode`, `country_id`, `facebook_profile`, `twitter_profile`, `google_plus_profile`, `linkedin_profile`, `created_at`, `updated_at`) VALUES
(1, 1, 'ram', 'tricky', NULL, NULL, 'male', 'uploads/avatar/5baa0eccd130b.jpg', NULL, '2018-09-13', '2018-09-21', NULL, NULL, NULL, NULL, NULL, NULL, '123', 'dd', '213', 'dddddddd', '2018-09-21 05:53:39', '2018-09-25 04:32:44'),
(2, 2, 'ramprasad', 'pendimi', NULL, NULL, NULL, 'uploads/avatar/5ba8af401b415.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-09-21 14:57:41', '2018-09-24 09:32:48'),
(3, 3, 'ramprasad', 'pendimi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-09-21 14:58:32', '2018-09-21 14:58:32'),
(4, 4, 'tester', 'tester', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-09-22 06:48:24', '2018-09-22 06:48:24');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'web', '2018-09-21 00:18:37', '2018-09-21 00:18:37'),
(2, 'user', 'web', '2018-09-21 00:18:37', '2018-09-21 00:18:37');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(11, 2);

-- --------------------------------------------------------

--
-- Table structure for table `todos`
--

CREATE TABLE `todos` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '-1',
  `date` date DEFAULT NULL,
  `completed_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `todos`
--

INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `status`, `date`, `completed_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'todo1', 'test', 0, '2018-09-14', NULL, '2018-09-21 11:44:57', '2018-09-21 11:44:57'),
(2, 1, 'todo 2', 'tested', 0, '2018-09-22', NULL, '2018-09-21 11:53:30', '2018-09-21 11:53:30'),
(3, 1, 'test 2', 'testing', 1, '2018-09-26', '2018-09-21 20:25:02', '2018-09-21 11:53:44', '2018-09-21 14:55:02'),
(4, 1, 'test 3', 'testing', 1, '2018-09-29', '2018-09-21 23:00:49', '2018-09-21 17:30:21', '2018-09-21 17:30:49'),
(5, 1, 'test 4', '', 0, '2018-09-25', NULL, '2018-09-21 17:30:33', '2018-09-21 17:31:09'),
(6, 1, 'test 5', '', 1, '2018-09-27', '2018-09-22 08:49:38', '2018-09-22 06:49:26', '2018-09-22 06:49:38'),
(7, 1, 'todo 6', '', 1, '2018-09-26', '2018-09-22 08:49:45', '2018-09-22 06:49:35', '2018-09-22 06:49:45'),
(8, 1, 'Bestrating GRATIS AFHALEN 20 m2', 'Bestrating GRATIS AFHALEN 20 m2', -1, '2018-09-05', NULL, '2018-09-24 09:39:54', '2018-09-24 09:39:54'),
(9, 1, '123', '123', 0, '2018-09-04', NULL, '2018-09-24 10:55:20', '2018-09-24 10:06:25');

-- --------------------------------------------------------

--
-- Table structure for table `uploads`
--

CREATE TABLE `uploads` (
  `id` int(10) UNSIGNED NOT NULL,
  `uuid` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `module` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `module_id` int(11) DEFAULT NULL,
  `upload_token` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_filename` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `filename` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_temp_delete` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `uploads`
--

INSERT INTO `uploads` (`id`, `uuid`, `user_id`, `module`, `module_id`, `upload_token`, `user_filename`, `filename`, `is_temp_delete`, `status`, `created_at`, `updated_at`) VALUES
(1, '51cd9222-cc83-4e51-bf07-ff5febdbfa94', 1, 'message', 1, '9e7d06ea-9995-40ae-be22-4385e5c36481', '13.jpg', 'uploads/message/lP7wQAI4hqTYFPNhJYt3qJtHplNuW5fTPQeo4cKJ.jpeg', 0, 1, '2018-09-24 13:30:48', '2018-09-24 13:31:10');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activation_token` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `activation_token`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'a@dmin.com', '$2y$10$bGIga.oDT1501HuKAlFkFuR2hkpoMRlaVEkncLKa.ceKJEXOURXWy', '3493e49e-60e8-4c3d-81f5-c9adbaa40bd8', 'activated', NULL, '2018-09-21 05:53:37', '2018-09-24 06:48:16'),
(2, 'user@financierdirect.nl', '$2y$10$ePz2xlhtVypJqXUe2TQcMOXPOkrab0u9.YNRz7DfsIX6nLhdEtati', '5a751a5f-6dfa-4036-9ce3-a6eb1168b2f6', 'activated', NULL, '2018-09-21 14:57:41', '2018-09-24 06:52:54'),
(3, 'ramprasadp48@gmail.com', '$2y$10$w7D5OX7v6SKtcYjdvHT31u.VbrkljWKguqgIkgHmuSm2zsFN1NJHe', '4aab6eb3-01d8-4091-bed8-1654728845b2', 'activated', NULL, '2018-09-21 14:58:32', '2018-09-21 14:58:32'),
(4, 'tester@tester.com', '$2y$10$w25iINl32eoNPP/gTWxu6.gWQERkSdCMiMkn49h5Vx.q50Tkz.sQC', 'dc5d7653-aab3-4cd8-8021-22393176a8fe', 'activated', NULL, '2018-09-22 06:48:24', '2018-09-22 06:48:24');

-- --------------------------------------------------------

--
-- Table structure for table `user_preferences`
--

CREATE TABLE `user_preferences` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sidebar` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `direction` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color_theme` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_preferences`
--

INSERT INTO `user_preferences` (`id`, `user_id`, `locale`, `sidebar`, `direction`, `color_theme`, `created_at`, `updated_at`) VALUES
(1, 1, 'en', 'mini', 'ltr', 'red', '2018-09-21 05:53:39', '2018-09-25 01:22:06'),
(2, 2, 'en', 'mini', 'ltr', 'green', '2018-09-21 14:57:41', '2018-09-24 09:32:22'),
(3, 3, NULL, NULL, NULL, NULL, '2018-09-21 14:58:32', '2018-09-21 14:58:32'),
(4, 4, NULL, NULL, NULL, NULL, '2018-09-22 06:48:24', '2018-09-22 06:48:24');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity_logs`
--
ALTER TABLE `activity_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `activity_logs_user_id_foreign` (`user_id`),
  ADD KEY `activity_logs_login_as_user_id_foreign` (`login_as_user_id`);

--
-- Indexes for table `backups`
--
ALTER TABLE `backups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `backups_user_id_foreign` (`user_id`);

--
-- Indexes for table `chats`
--
ALTER TABLE `chats`
  ADD PRIMARY KEY (`id`),
  ADD KEY `chats_user_id_foreign` (`user_id`);

--
-- Indexes for table `config`
--
ALTER TABLE `config`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_logs`
--
ALTER TABLE `email_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_templates`
--
ALTER TABLE `email_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ip_filters`
--
ALTER TABLE `ip_filters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_index` (`queue`);

--
-- Indexes for table `locales`
--
ALTER TABLE `locales`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `messages_from_user_id_foreign` (`from_user_id`),
  ADD KEY `messages_to_user_id_foreign` (`to_user_id`),
  ADD KEY `messages_reply_id_foreign` (`reply_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_type_model_id_index` (`model_type`,`model_id`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_type_model_id_index` (`model_type`,`model_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `proefritten`
--
ALTER TABLE `proefritten`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `profiles_user_id_foreign` (`user_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `todos`
--
ALTER TABLE `todos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `todos_user_id_foreign` (`user_id`);

--
-- Indexes for table `uploads`
--
ALTER TABLE `uploads`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uploads_user_id_foreign` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_preferences`
--
ALTER TABLE `user_preferences`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_preferences_user_id_foreign` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity_logs`
--
ALTER TABLE `activity_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;

--
-- AUTO_INCREMENT for table `backups`
--
ALTER TABLE `backups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `chats`
--
ALTER TABLE `chats`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `config`
--
ALTER TABLE `config`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `email_logs`
--
ALTER TABLE `email_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `email_templates`
--
ALTER TABLE `email_templates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `ip_filters`
--
ALTER TABLE `ip_filters`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `locales`
--
ALTER TABLE `locales`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `proefritten`
--
ALTER TABLE `proefritten`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `profiles`
--
ALTER TABLE `profiles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `todos`
--
ALTER TABLE `todos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `uploads`
--
ALTER TABLE `uploads`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user_preferences`
--
ALTER TABLE `user_preferences`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `activity_logs`
--
ALTER TABLE `activity_logs`
  ADD CONSTRAINT `activity_logs_login_as_user_id_foreign` FOREIGN KEY (`login_as_user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `activity_logs_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `backups`
--
ALTER TABLE `backups`
  ADD CONSTRAINT `backups_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `chats`
--
ALTER TABLE `chats`
  ADD CONSTRAINT `chats_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `messages`
--
ALTER TABLE `messages`
  ADD CONSTRAINT `messages_from_user_id_foreign` FOREIGN KEY (`from_user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `messages_reply_id_foreign` FOREIGN KEY (`reply_id`) REFERENCES `messages` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `messages_to_user_id_foreign` FOREIGN KEY (`to_user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `profiles`
--
ALTER TABLE `profiles`
  ADD CONSTRAINT `profiles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `todos`
--
ALTER TABLE `todos`
  ADD CONSTRAINT `todos_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `uploads`
--
ALTER TABLE `uploads`
  ADD CONSTRAINT `uploads_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_preferences`
--
ALTER TABLE `user_preferences`
  ADD CONSTRAINT `user_preferences_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
